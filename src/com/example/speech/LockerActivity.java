package com.example.speech;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;


public class LockerActivity extends Activity implements
RecognitionListener{
	private static SpeechRecognizer speech = null;
	private static Intent recognizerIntent;
	//private Switch mySwitch;
    private Button bvoice;
    private Button fvoice;
    private ToggleButton toggle;
	final Context context = this;
	private ProgressDialog progressBar;    
	
	@Override
	public void onResume() {
    	super.onResume();
	}

    	
    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		bvoice = (Button) findViewById(R.id.button1);
		fvoice = (Button) findViewById(R.id.button2);
		toggle = (ToggleButton)findViewById(R.id.toggleButton1);
		toggle.setChecked(false);
		    
		progressBar = new ProgressDialog(this);
		OnCheckedChangeListener listener = new OnCheckedChangeListener() {
	        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	            if(isChecked) {
	            	bvoice.setClickable(true);
	            	fvoice.setClickable(true);
	            	Thread t = new Thread(){
	            		public void run(){
	            			ContextWrapper cont = new ContextWrapper(getBaseContext()); cont.startService(new Intent(LockerActivity.this,MyService.class));
	            		}
	            		};
	            		t.start();
	            	//startService(new Intent(LockerActivity.this,MyService.class));
			    	
				     Toast.makeText(getApplicationContext(), "Locker enabled.", 
						      Toast.LENGTH_SHORT).show();	
	            } else {
	            	bvoice.setClickable(false);
	            	fvoice.setClickable(false);
	            	stopService(new Intent(LockerActivity.this,MyService.class));
			    	//unregisterReceiver(mReceiver);
			    	
			    	Toast.makeText(getApplicationContext(), "Locker disabled.", 
						      Toast.LENGTH_SHORT).show();	
	            }
	        }
	    };
		
	    toggle.setOnCheckedChangeListener(listener);		
	    
		bvoice.setOnClickListener(new View.OnClickListener() {
	           public void onClick(View v) {
	        	   	speak();
	           }
	        });
		
		fvoice.setOnClickListener(new View.OnClickListener() {
	           public void onClick(View v) {
	        	   
	           }
	        });
	 					
	}
    
    public void speak()
	{
		speech = SpeechRecognizer.createSpeechRecognizer(this);
		speech.setRecognitionListener(this);
		recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
				"en");
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
				this.getPackageName());
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
		
		progressBar.setMessage("Enter Voice.... ");
		progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setIndeterminate(true);
		progressBar.show();
		
		speech.startListening(recognizerIntent);
		
		
	}
    
   
	@Override
	protected void onPause() {
		super.onPause();
		if (speech != null) {
			speech.destroy();
			//Log.i(LOG_TAG, "destroy");
		}

	}

	@Override
	public void onBeginningOfSpeech() {
		//Log.i(LOG_TAG, "onBeginningOfSpeech");
		progressBar.setIndeterminate(false);
		progressBar.setMax(10);
	}

	@Override
	public void onBufferReceived(byte[] buffer) {
		//Log.i(LOG_TAG, "onBufferReceived: " + buffer);
	}

	@Override
	public void onEndOfSpeech() {
		//Log.i(LOG_TAG, "onEndOfSpeech");
		progressBar.setIndeterminate(true);
		//toggleButton.setChecked(false);
	}

	@Override
	public void onError(int errorCode) {
		String errorMessage = getErrorText(errorCode);
		Toast.makeText(getApplicationContext(), errorMessage, 
			      Toast.LENGTH_SHORT).show();
		//Log.d(LOG_TAG, "FAILED " + errorMessage);
		//returnedText.setText(errorMessage);
		//toggleButton.setChecked(false);
	}

	@Override
	public void onEvent(int arg0, Bundle arg1) {
		//Log.i(LOG_TAG, "onEvent");
	}

	@Override
	public void onPartialResults(Bundle arg0) {
		//Log.i(LOG_TAG, "onPartialResults");
	}

	@Override
	public void onReadyForSpeech(Bundle arg0) {
		//Log.i(LOG_TAG, "onReadyForSpeech");
	}

	@Override
	public void onResults(Bundle results) {
		//Log.i(LOG_TAG, "onResults");
		final ArrayList<String> matches = results
				.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
		
		//speak();
		if(matches.isEmpty())
		{
			speak();
		}
		else
		{			
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
 
			// set title
			alertDialogBuilder.setTitle("Set New Voice-Key.");
 
			// set dialog message
			alertDialogBuilder
				.setMessage("Do You want to set "+"\""+matches.get(0)+"\""+" as Voice Key")
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						String FILENAME = "voice";
					
						FileOutputStream fos = null;
						try {
							fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
						try {
							fos.write(matches.get(0).getBytes());
						} catch (IOException e) {
							e.printStackTrace();
						}
						try {
							fos.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						
						progressBar.setIndeterminate(false);
						progressBar.dismiss();
						speech.stopListening();
						
						
					}
				  })
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						
						dialog.cancel();
						speak();
						
					}
				});
 
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
 
				// show it
				alertDialog.show();
		}
	}

	@Override
	public void onRmsChanged(float rmsdB) {
		//Log.i(LOG_TAG, "onRmsChanged: " + rmsdB);
		progressBar.setProgress((int) rmsdB);
	}

	public String getErrorText(int errorCode) {
		String message;
		switch (errorCode) {
		case SpeechRecognizer.ERROR_AUDIO:
			message = "Audio recording error";
			break;
		case SpeechRecognizer.ERROR_CLIENT:
			message = "Client side error";
			break;
		case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
			message = "Insufficient permissions";
			break;
		case SpeechRecognizer.ERROR_NETWORK:
			message = "Network error";
			break;
		case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
			message = "Network timeout";
			break;
		case SpeechRecognizer.ERROR_NO_MATCH:
			message = "No match";speak();
			break;
		case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
			message = "RecognitionService busy";
			break;
		case SpeechRecognizer.ERROR_SERVER:
			message = "error from server";
			break;
		case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
			message = "No speech input";speak();
			break;
		default:
			message = "Didn't understand, please try again.";speak();
			break;
		}
		return message;
	}
	


}
