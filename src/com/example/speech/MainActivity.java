package com.example.speech;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.example.speech.R;
import com.example.speech.AnimatedGifImageView;
import com.example.speech.AnimatedGifImageView.TYPE;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.Toast;

public class MainActivity extends Activity implements
RecognitionListener {
	private AnimatedGifImageView animatedGifImageView;
	
	private static SpeechRecognizer speech = null;
	private static Intent recognizerIntent;
	//private static final int VOICE_RECOGNITION_REQUEST_CODE = 1001;
	 @Override
	 public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG|WindowManager.LayoutParams.FLAG_FULLSCREEN);

          super.onAttachedToWindow();
	 }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
 	   getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|WindowManager.LayoutParams.FLAG_FULLSCREEN);
 	  if(getIntent()!=null&&getIntent().hasExtra("kill")&&getIntent().getExtras().getInt("kill")==1){
	      // Toast.makeText(this, "" + "kill activityy", Toast.LENGTH_SHORT).show();
	        	finish();
	    	}
 	  //startService(new Intent(this,MyService.class));
		setContentView(R.layout.activity_main);
		animatedGifImageView = ((AnimatedGifImageView)findViewById(R.id.animatedGifImageView));
		animatedGifImageView.setAnimatedGif(R.raw.spark,
				TYPE.FIT_CENTER);
		
		//progressBar = new ProgressDialog(this);
		
		speak();
		
		
		StateListener phoneStateListener = new StateListener();
        TelephonyManager telephonyManager =(TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener,PhoneStateListener.LISTEN_CALL_STATE);

	}

	public void checkVoiceRecognition() {
		// Check if voice recognition is present
		PackageManager pm = getPackageManager();
		List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(
				RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
		if (activities.size() == 0) {
			//mbtSpeak.setEnabled(false);
			Toast.makeText(this, "Voice recognizer not present",
					Toast.LENGTH_SHORT).show();
		}
	}

	public void speak()
	{
		speech = SpeechRecognizer.createSpeechRecognizer(this);
		speech.setRecognitionListener(this);
		recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
				"en");
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
				this.getPackageName());
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
		
		speech.startListening(recognizerIntent);
		
		
	}
	
	/*public void speak() {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		// Specify the calling package to identify your application
		intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass()
				.getPackage().getName());

		// Display an hint to the user about what he should say.
		
		// Given an hint to the recognizer about what the user is going to say
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);

		startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
	}*/
	
/*	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == VOICE_RECOGNITION_REQUEST_CODE)

			//If Voice recognition is successful then it returns RESULT_OK
			if(resultCode == RESULT_OK) {

				ArrayList<String> textMatchList = data
				.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				if (!textMatchList.isEmpty()) {
					// If first Match contains the 'search' word
					// Then start web search.

					 // populate the Matches
					if(textMatchList.get(0).equals("unlock"))
					{
						Intent startMain = new Intent(Intent.ACTION_MAIN);
						startMain.addCategory(Intent.CATEGORY_HOME);
						startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(startMain);
						
					}
					else
					{speak();}
					 Toast.makeText(getApplicationContext(), textMatchList.get(0), 
						      Toast.LENGTH_SHORT).show();
		
				}
			//Result code for various error.	
			}else if(resultCode == RecognizerIntent.RESULT_AUDIO_ERROR){
				showToastMessage("Audio Error");
			}else if(resultCode == RecognizerIntent.RESULT_CLIENT_ERROR){
				showToastMessage("Client Error");
			}else if(resultCode == RecognizerIntent.RESULT_NETWORK_ERROR){
				showToastMessage("Network Error");
			}else if(resultCode == RecognizerIntent.RESULT_NO_MATCH){
				showToastMessage("No Match");
			}else if(resultCode == RecognizerIntent.RESULT_SERVER_ERROR){
				showToastMessage("Server Error");
			}
		super.onActivityResult(requestCode, resultCode, data);
	}*/
	
	void showToastMessage(String message){
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}
	
	 class StateListener extends PhoneStateListener{
	        @Override
	        public void onCallStateChanged(int state, String incomingNumber) {

	            super.onCallStateChanged(state, incomingNumber);
	            switch(state){
	                case TelephonyManager.CALL_STATE_RINGING:
	                    break;
	                case TelephonyManager.CALL_STATE_OFFHOOK:
	                    System.out.println("call Activity off hook");
	                	finish();
	                	break;
	                case TelephonyManager.CALL_STATE_IDLE:
	                    break;
	            }
	        }
	    };

    @Override
    public void onBackPressed() {
        // Don't allow back to dismiss.
        return;
    }

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (speech != null) {
			speech.destroy();
			//Log.i(LOG_TAG, "destroy");
		}

	}

	@Override
	public void onBeginningOfSpeech() {
		//Log.i(LOG_TAG, "onBeginningOfSpeech");
		//progressBar.setIndeterminate(false);
		//progressBar.setMax(10);
	}

	@Override
	public void onBufferReceived(byte[] buffer) {
		//Log.i(LOG_TAG, "onBufferReceived: " + buffer);
	}

	@Override
	public void onEndOfSpeech() {
		//Log.i(LOG_TAG, "onEndOfSpeech");
		//progressBar.setIndeterminate(true);
		//toggleButton.setChecked(false);
	}

	@Override
	public void onError(int errorCode) {
		String errorMessage = getErrorText(errorCode);
		Toast.makeText(getApplicationContext(), errorMessage, 
			      Toast.LENGTH_SHORT).show();
		//Log.d(LOG_TAG, "FAILED " + errorMessage);
		//returnedText.setText(errorMessage);
		//toggleButton.setChecked(false);
	}

	@Override
	public void onEvent(int arg0, Bundle arg1) {
		//Log.i(LOG_TAG, "onEvent");
	}

	@Override
	public void onPartialResults(Bundle arg0) {
		//Log.i(LOG_TAG, "onPartialResults");
	}

	@Override
	public void onReadyForSpeech(Bundle arg0) {
		//Log.i(LOG_TAG, "onReadyForSpeech");
	}

	@Override
	public void onResults(Bundle results) {
		//Log.i(LOG_TAG, "onResults");
		ArrayList<String> matches = results
				.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
		String temp="";
		FileInputStream fin = null;
		try {
			fin = openFileInput("voice");
			int c;
			
			try {
				while( (c = fin.read()) != -1){
				   temp = temp + Character.toString((char)c);
				}
			}
			catch (IOException e) {
				Toast.makeText(getApplicationContext(), e.toString(), 
					      Toast.LENGTH_SHORT).show();
			}
		}
		catch (FileNotFoundException e) {
			Toast.makeText(getApplicationContext(), e.toString(), 
				      Toast.LENGTH_SHORT).show();
		}
		
		if(matches.get(0).equals(temp))
		{
			//progressBar.setIndeterminate(false);
			//progressBar.dismiss();
			speech.stopListening();
			//this.finishActivity(TRIM_MEMORY_COMPLETE);
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			this.finish();
			
		}
		else
		{
			speak();
			Toast.makeText(getApplicationContext(), "Wrong VoiceKey try again!", 
				      Toast.LENGTH_SHORT).show();
		}
		 
	}

	@Override
	public void onRmsChanged(float rmsdB) {
		//Log.i(LOG_TAG, "onRmsChanged: " + rmsdB);
		//progressBar.setProgress((int) rmsdB);
	}

	public String getErrorText(int errorCode) {
		String message;
		switch (errorCode) {
		case SpeechRecognizer.ERROR_AUDIO:
			message = "Audio recording error";
			break;
		case SpeechRecognizer.ERROR_CLIENT:
			message = "Client side error";
			break;
		case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
			message = "Insufficient permissions";
			break;
		case SpeechRecognizer.ERROR_NETWORK:
			message = "Network error";
			break;
		case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
			message = "Network timeout";
			break;
		case SpeechRecognizer.ERROR_NO_MATCH:
			message = "No match";speak();
			break;
		case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
			message = "RecognitionService busy";
			break;
		case SpeechRecognizer.ERROR_SERVER:
			message = "error from server";
			break;
		case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
			message = "No speech input";speak();
			break;
		default:
			message = "Didn't understand, please try again.";speak();
			break;
		}
		return message;
	}
	
    @Override
  public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {

    	if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)||(keyCode == KeyEvent.KEYCODE_POWER)||(keyCode == KeyEvent.KEYCODE_VOLUME_UP)||(keyCode == KeyEvent.KEYCODE_CAMERA)) {
    	    //this is where I can do my stuff
    	    return true; //because I handled the event
    	}
       if((keyCode == KeyEvent.KEYCODE_HOME)){

    	   return true;
        }

	return false;

    }

    public boolean dispatchKeyEvent(KeyEvent event) {
    	if (event.getKeyCode() == KeyEvent.KEYCODE_POWER ||(event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN)||(event.getKeyCode() == KeyEvent.KEYCODE_POWER)) {
    	    //Intent i = new Intent(this, NewActivity.class);
    	    //startActivity(i);
    	    return false;
    	}
    	 if((event.getKeyCode() == KeyEvent.KEYCODE_HOME)){

     	   return true;
         }
    return false;
    }

    public void onDestroy(){
       // k1.reenableKeyguard();

        super.onDestroy();
    }
	
	
}